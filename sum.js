// Sample
function sum(a, b) {
  return a + b;
}

function multi(c, d) {
  return c * d;
}

function sub(e, f) {
  return e - f;
}

module.exports = { sum, sub };
